# Esta es una plantilla para practicar en clase


## Informe de seguridad para SISTEMX

Tras la auditoría, se ha detectado que el sistema es [seguro/algo inseguro/muy inseguro] porque [mi motivo]. 

Las categorías de OWASP que se han analizado son:

- Categoria A
- Categoria B
...

Las recomendaciones de seguridad son las siguientes:

- Recomendación X...



